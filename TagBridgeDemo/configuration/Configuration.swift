//
//  Configuration.swift
//  TagBridgeDemo
//
//  Created by Christina on 9/29/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation

enum TealiumConfiguration {
    static let account = "services-christina"
    static let profile = "tagbridge"
    static let environment = "prod"
    static let datasource = ""
}

enum CustomCommandConfiguration {
    enum CommandIds {
        static let experienceA = "experienceA"
        static let experienceB = "experienceB"
        static let weather = "weather"
    }
    enum Descriptions {
        static let experienceA = "This will display the UI for experience A"
        static let experienceB = "This will display the UI for experience B"
        static let weather = "This will display the response from the weather service"
    }
}
