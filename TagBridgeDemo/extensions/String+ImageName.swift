//
//  String.swift
//  TagBridgeDemo
//
//  Created by Christina on 9/30/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation

extension String {
    
    func isImageName() -> Bool {
        let pattern = "(jpg|png|gif|bmp)$"
        let matchesFound = Regex.matchesFound(forPattern: pattern,
                      in: self)
        if matchesFound != 0 {
            return true
        } else {
            return false
        }
    }
    
    func addHashIfNone() -> String {
        if self.hasPrefix("#") {
            return self
        } else {
            return "#\(self)"
        }
    }
    
}
