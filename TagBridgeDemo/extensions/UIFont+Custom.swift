//
//  UIFont+Custom.swift
//  TagBridgeDemo
//
//  Created by Christina on 10/22/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

extension UIFont {
    
    enum Demo {
        static let heavyLarge: UIFont = UIFont.systemFont(ofSize: 20.0, weight: .heavy)
        static let heavySmall: UIFont = UIFont.systemFont(ofSize: 17.0, weight: .heavy)
        static let lightLarge: UIFont = UIFont.systemFont(ofSize: 17.0, weight: .light)
        static let lightSmall: UIFont = UIFont.systemFont(ofSize: 15.0, weight: .light)
    }
    
}
