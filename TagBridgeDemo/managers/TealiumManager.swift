//
//  TealiumManager.swift
//  TagBridgeDemo
//
//  Created by Christina on 9/28/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation
import tealium_swift


class TealiumManager {

    var tealium: Tealium?
    static var shared = TealiumManager()
    var loggingIsEnabled = true
    
    func start() {
        let config = TealiumConfig(account: TealiumConfiguration.account,
                                   profile: TealiumConfiguration.profile,
                                   environment: TealiumConfiguration.environment,
                                   datasource: TealiumConfiguration.datasource,
                                   optionalData: nil)
        config.setLogLevel(logLevel: .verbose)
        config.addDelegate(self)
        let list = TealiumModulesList(isWhitelist: false,
                                      moduleNames: ["autotracking", "collect", "consentmanager"])
        config.setModulesList(list)
        tealium = Tealium(config: config,
                          completion: { (responses) in
        })
    }
    
    func addVolitileData(data: [String: Any]) {
        self.tealium?.volatileData()?.add(data: data)
    }
    
    func track(title: String, data: [String: Any]?) {
        tealium?.track(title: title,
                       data: data,
                       completion: { (success, info, error) in
                        if self.loggingIsEnabled == true {
                            Log.debug("*** TealiumHelper: track completed:\n\(success)\n\(String(describing: info))\n\(String(describing: error))")
                        }
        })
    }
    
    func trackView(title: String, data: [String: Any]?) {
        tealium?.trackView(title: title,
                           data: data,
                           completion: { (success, info, error) in
                            if self.loggingIsEnabled == true {
                                Log.debug("*** TealiumHelper: view completed:\n\(success)\n\(String(describing: info))\n\(String(describing: error))")
                            }
                    })
    }
    
}

extension TealiumManager: TealiumDelegate {
    
    func tealiumShouldTrack(data: [String: Any]) -> Bool {
        return true
    }
    
    func tealiumTrackCompleted(success: Bool, info: [String: Any]?, error: Error?) {
        if self.loggingIsEnabled == true {
            Log.debug("\n*** Tealium Helper: Tealium Delegate : tealiumTrackCompleted *** Track finished. Was successful:\(success)\nInfo:\(info as AnyObject)\((error != nil) ? "\nError:\(String(describing: error))" : "")")
        }
    }
    
}
