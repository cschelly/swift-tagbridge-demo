//
//  CustomCommand.swift
//  TagBridgeDemo
//
//  Created by Christina on 9/29/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation

struct CustomCommand {
    
    let commandId: String
    let description: String?
    
}
