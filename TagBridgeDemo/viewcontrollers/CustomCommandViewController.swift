//
//  CustomCommandViewController.swift
//  TagBridgeDemo
//
//  Created by Christina on 9/28/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit
import tealium_swift

class CustomCommandViewController: UIViewController {

    @IBOutlet weak var experienceIndicatorText: UILabel!
    @IBOutlet weak var appGreeting: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var experienceAButton: UIButton!
    @IBOutlet weak var experienceBButton: UIButton!
    @IBOutlet weak var experienceDescription: UILabel!
    @IBOutlet weak var experienceImage: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func experienceAButtonTapped(_ sender: Any?) {
        experienceLabel.text = "A"
        TealiumManager.shared.track(title: "experienceA", data: nil)
        let aDataViewModel = FetchDataViewModel(forExperience: "A")
        setupDataViewModel(with: aDataViewModel)
    }

    @IBAction func experienceBButtonTapped(_ sender: Any?) {
        self.experienceLabel.text = "B"
        TealiumManager.shared.track(title: "experienceB", data: nil)
        let bDataViewModel = FetchDataViewModel(forExperience: "B")
        setupDataViewModel(with: bDataViewModel)
    }

    private func setupDataViewModel(with viewModel: FetchDataViewModel) {
        viewModel.didFetchData = { [weak self] (customCommandData, error) in
            if error != nil {
                Log.error("Could not fetch data")
            }
            if let data = customCommandData {
                let dataViewModel = CustomCommandDataViewModel(incomingData: data)
                self?.experienceLabel.textColor = dataViewModel.textColor
                self?.experienceImage.image = dataViewModel.image
                self?.experienceDescription.text = dataViewModel.description
                self?.appGreeting.textColor = dataViewModel.textColor
                self?.experienceIndicatorText.textColor = dataViewModel.textColor
                self?.experienceDescription.textColor = dataViewModel.textColor
                self?.experienceLabel.textColor = dataViewModel.textColor
                self?.experienceAButton.setTitleColor(dataViewModel.textColor, for: .normal)
                self?.experienceBButton.setTitleColor(dataViewModel.textColor, for: .normal)
                if let backgroundColor = dataViewModel.backgroundImageOrColorString.color {
                    self?.view.backgroundColor = backgroundColor
                } else if let backgroundImage = dataViewModel.backgroundImageOrColorString.image {
                   self?.view.backgroundColor = UIColor(patternImage: backgroundImage)
                }
                TealiumManager.shared.tealium?.remoteCommands()?.remove(commandWithId: dataViewModel.commandId)
            }
        }
    }

}
