//
//  HTTPResponseViewController.swift
//  TagBridgeDemo
//
//  Created by Christina on 9/28/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit
import tealium_swift
import CoreLocation

enum HTTPResponseErrors: Error {
    case unableToRetrieveLatitude
    case unableToRetrieveLongitude
}

enum HTTPResponseLocationKeys {
    static var latitude = "latitude"
    static var longitude = "longitude"
}

class HTTPResponseViewController: UIViewController, CLLocationManagerDelegate {

    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var temperaturelabel: UILabel!
    @IBOutlet var humidityLabel: UILabel!
    @IBOutlet var windSpeedLabel: UILabel!
    @IBOutlet var summaryLabel: UILabel!
    @IBOutlet weak var testingLabel: UILabel!
    
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?

    @IBOutlet var dateLabel: UILabel? {
        didSet {
            dateLabel?.textColor = UIColor.Demo.baseTextColor
            dateLabel?.font =  UIFont.Demo.heavyLarge
        }
    }
    
    @IBOutlet var regularLabels: [UILabel]! {
        didSet {
            for label in regularLabels {
                label.textColor = .black
                label.font = UIFont.Demo.heavySmall
            }
        }
    }
    
    @IBOutlet var smallLabels: [UILabel]! {
        didSet {
            for label in smallLabels {
                label.textColor = .black
                label.font = UIFont.Demo.lightSmall
            }
        }
    }
    
    @IBOutlet var iconImageView: UIImageView! {
        didSet {
            iconImageView.contentMode = .scaleAspectFit
            iconImageView.tintColor = UIColor.Demo.baseTintColor
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {
            currentLocation = locationManager.location

            guard let latitude = currentLocation?.coordinate.latitude else {
                Log.error(HTTPResponseErrors.unableToRetrieveLatitude)
                return
            }
            guard let longitude = currentLocation?.coordinate.longitude else {
                Log.error(HTTPResponseErrors.unableToRetrieveLongitude)
                return
            }
            TealiumManager.shared.trackView(title: "location_enabled", data: [HTTPResponseLocationKeys.latitude: latitude, HTTPResponseLocationKeys.longitude: longitude])
            self.dateLabel?.text = "Current Weather"
        } else {
            TealiumManager.shared.trackView(title: "no_location", data: nil)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
//        let currentLocationViewModel = FetchDataViewModel(forExperience: "weather")
//        setupDataViewModel(with: currentLocationViewModel)
    }
    
    private func setupView() {
        view.backgroundColor = UIColor.Demo.lightBackgroundColor
    }

    private func setupDataViewModel(with viewModel: FetchDataViewModel) {
        viewModel.didFetchData = { [weak self] (weatherData, error) in
            if error != nil {
                Log.error("Could not fetch data")
            }
            if let data = weatherData {
                let dataViewModel = HTTPResponseViewModel(incomingData: data)
                self?.dateLabel?.text = dataViewModel.date
                self?.timeLabel?.text = dataViewModel.time
                self?.temperaturelabel?.text = dataViewModel.temperature
                self?.windSpeedLabel?.text = dataViewModel.windSpeed
                self?.humidityLabel?.text = "4 MPH"
                self?.summaryLabel?.text = dataViewModel.summary
                self?.iconImageView?.image = dataViewModel.image
                TealiumManager.shared.tealium?.remoteCommands()?.remove(commandWithId: dataViewModel.commandId)
            }
        }
    }
    
    @IBAction func displayWeatherData(_ sender: Any) {
        TealiumManager.shared.track(title: "display_data", data: nil)
        let weatherDataDisplay = TealiumRemoteCommand(commandId: "weather", description: "test") { (response) in
            print("WeatherData: \(response)")
            let payload = response.payload()
            if let summary = payload["current_summary"] as? String {
                self.testingLabel?.text = summary
            }
        }
        TealiumManager.shared.tealium?.remoteCommands()?.add(weatherDataDisplay)
    }
}

