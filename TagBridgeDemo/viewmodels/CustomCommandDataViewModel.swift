//
//  CustomCommandDataViewModel.swift
//  TagBridgeDemo
//
//  Created by Christina on 9/29/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

struct CustomCommandDataViewModel {
    
    let incomingData: [String:Any]

    typealias backgroundImageOrColor = (image: UIImage?, color: UIColor?)
    
    var background: String {
        guard let background = incomingData["background"] as? String else {
            return ""
        }
        return background
    }
    
    var backgroundImageOrColorString: backgroundImageOrColor {
        let imgExtension = (background as NSString).pathExtension
        if (imgExtension.isImageName()) {
            return (UIImage(named: background),nil)
        } else {
            return (nil, UIColor(hexString: background.addHashIfNone()))
        }
    }
    
    var image: UIImage? {
        guard let imageName = incomingData["image_name"] as? String else {
            return nil
        }
        return  UIImage(named: imageName)
    }
    
    var textColor: UIColor? {
        guard let textColor = incomingData["text_color"] as? String else {
            return nil
        }
        return UIColor(hexString: textColor.addHashIfNone())
    }
    
    var description: String {
        guard let description = incomingData["description"] as? String else {
            return ""
        }
        return description
    }
    
    var commandId: String {
        if let commandId = incomingData["command_id"] as? String {
            return commandId
        } else if let commandId = incomingData["mapped_data.command_id"] as? String {
            return commandId
        } else {
            return ""
        }
    }
    
}
