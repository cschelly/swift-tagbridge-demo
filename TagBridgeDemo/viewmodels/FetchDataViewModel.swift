//
//  FetchDataViewModel.swift
//  TagBridgeDemo
//
//  Created by Christina on 9/29/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import Foundation
import tealium_swift

class FetchDataViewModel {

    enum FetchDataError: Error {
        case noDataAvailable
        case dataKeyNotFoundInPayload
        case unexpectedDataType
        case customCommnadConfigurationNotFound
    }

    enum FetchDataCommandIds {
        static let a = "A"
        static let b = "B"
        static let weather = "weather"
    }
    
    typealias DidFetchDataCompletion = ([String: Any]?, FetchDataError?) -> Void

    var didFetchData: DidFetchDataCompletion?

    init(forExperience identifier: String) {
        fetchData(identifier: identifier)
    }

    private func fetchData(identifier: String) {

        var customCommand: CustomCommand

        switch identifier {
        case FetchDataCommandIds.a:
            customCommand = CustomCommand.init(commandId: CustomCommandConfiguration.CommandIds.experienceA, description: CustomCommandConfiguration.Descriptions.experienceA)
            break
        case FetchDataCommandIds.b:
            customCommand = CustomCommand.init(commandId: CustomCommandConfiguration.CommandIds.experienceB, description: CustomCommandConfiguration.Descriptions.experienceB)
            break
        case FetchDataCommandIds.weather:
            customCommand = CustomCommand.init(commandId: CustomCommandConfiguration.CommandIds.weather, description: CustomCommandConfiguration.Descriptions.weather)
            break
        default:
            Log.error("Custom command configuration not found")
            self.didFetchData?(nil, .customCommnadConfigurationNotFound)
            return
        }

        let command = TealiumRemoteCommand(commandId: customCommand.commandId, description: customCommand.description) { (response) in
            let customCommandData = response.payload()
            Log.debug("*****Remote Command Response: \(customCommandData)*****")
            self.didFetchData?(customCommandData, nil)
        }
        TealiumManager.shared.tealium?.remoteCommands()?.add(command)

    }

}
