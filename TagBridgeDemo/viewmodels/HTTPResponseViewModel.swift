//
//  HTTPResponseViewModel.swift
//  TagBridgeDemo
//
//  Created by Christina on 10/22/18.
//  Copyright © 2018 Christina. All rights reserved.
//

import UIKit

struct HTTPResponseViewModel {
    
    let incomingData: [String:Any]
    private let dateFormatter = DateFormatter()
    private let numberFormatter = NumberFormatter()


    var date: String {
        dateFormatter.dateFormat = "EEE, MMMM d YYYY"
        guard let time = incomingData["current_time"] as? Date else {
            return ""
        }
        return dateFormatter.string(from: time)
    }
    
    var time: String {
        dateFormatter.dateFormat = "hh:mm a"
        guard let time = incomingData["current_time"] as? Date else {
            return ""
        }
        return dateFormatter.string(from: time)
    }
    
    var summary: String {
        guard let summary = incomingData["current_summary"] as? String else {
            return ""
        }
        return summary
    }
    
    var temperature: String {
        guard let temperature = incomingData["current_temperature"] as? String else {
            return ""
        }
        return String(format: "%.1f °F", temperature)
    }
    
    var humidity: String {
        numberFormatter.numberStyle = .percent
        numberFormatter.locale = Locale(identifier: "en")
        guard let humidity = incomingData["current_humidity"] as? NSNumber else {
            return ""
        }
        return numberFormatter.string(from: humidity)!
    }
    
    var windSpeed: String {
        guard let windSpeed = incomingData["current_windSpeed"] as? String else {
            return ""
        }
        return String(format: "%.f MPH", windSpeed)
    }
    
    var image: UIImage? {
        guard let icon = incomingData["current_icon"] as? String else {
            return nil
        }
        return UIImage.imageForIcon(with: icon)
    }
    
    var commandId: String {
        if let commandId = incomingData["command_id"] as? String {
            return commandId
        } else if let commandId = incomingData["mapped_data.command_id"] as? String {
            return commandId
        } else {
            return ""
        }
    }
    
}
